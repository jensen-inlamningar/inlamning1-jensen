public class ovningar {

    public static void main(String[] args) {



        valueReturned();
        valueReturned1("Anton");
        noValue();
        numberOfLetter("Lite test text");
    }

    //1
    //Genom att skriva key ordet static så deklarerar man metoden som dess
    public static void staticMethod(){
        String text = "";
    }

    //2
    //Genom att använda sig av return i metoden så kan man returnera ett värde
    public int returnValue () {
        int num = 5;
        return num;
    }

    //3
    public static String valueReturned(){
        String name = "Sofia";
        return name;
    }
    public static String valueReturned1(String name){
        return name;
    }


    //4
    public static void noValue(){
        int value = 0;
    }

    //5
    public static String constant(){
        final String name = "Elsa";
        return name;
    }

    //Del 2

    public static void numberOfLetter(String testText){
        long count = testText.chars().filter(ch -> ch == 't').count();
        System.out.println(count);
    }


}
